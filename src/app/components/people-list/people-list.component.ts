import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/interfaces/person';
import { PeopleService } from 'src/app/services/people.service';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss']
})
export class PeopleListComponent implements OnInit {

  people: Person[] = [];
  selectedPerson: Person;

  constructor(public peopleService: PeopleService) {
    this.people = peopleService.getAll();
  }

  selectPerson(person) {
    this.selectedPerson = person;
  }

  ngOnInit() {
  }

}
