import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Person } from 'src/app/interfaces/person';
import { PeopleService } from 'src/app/services/people.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.scss']
})
export class PersonDetailsComponent implements OnInit, OnDestroy {

  sub: any;
  @Input() person: Person;

  constructor(
    public peopleService: PeopleService,
    public route: ActivatedRoute,
    public router: Router
  ) { }

  gotoPeoplesList() {
    const link = ['/persons'];
    this.router.navigate(link);
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = Number.parseInt(params['id']);
      this.person = this.peopleService.get(id);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
